<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oreninc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bu?$*nEpsogyP^~lduZ6F6|$oL,#XE!-M<&#.s3mxC[^?,p+-qM34]es-eltF:0t');
define('SECURE_AUTH_KEY',  'Cc=HJmTWQKB>k1/pMt06he7rtX4u)-[1W:TnL3)$4*eT SUH?:oIj5l@-|~uDXyj');
define('LOGGED_IN_KEY',    'm8kb~uLnbN6Ung$69iay1c+V0PA^o6i0<!6e,T(MBD~[|_PrYQ]S+CUke5[lWqxH');
define('NONCE_KEY',        'ZtYcz>L[|Qt2vX4yue42)d/DS99o>WV>+U#FknbG-o52u~SX)&_*X/b0uY,p9y%3');
define('AUTH_SALT',        '9N+-;Wej?O_^x <- ~(7a~d]p_j7Sw=z,xQK^fl5c_ey`+*aaUF50 67`Gy2EK73');
define('SECURE_AUTH_SALT', 'IrMc QE$ZK+i`#?/@6NVMLh&M1+4tP,es%9GhE:7Phgv~BzNx*mcHoQ>C2w+`8}{');
define('LOGGED_IN_SALT',   '[9G=v?y.P9$,SNRkJcL-g![sTaQl!5g7}9=#i{cMv$l%It-!%b?>`@#[pR.#+#X-');
define('NONCE_SALT',       ':1$3An8VrIo(zl{s?-uL89ISp ]slCKwpSr;0yI3G<W)50&[[S4;1{PS*+]*M^`t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
