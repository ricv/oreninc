<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package oreninc
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer bg-oreninc-inv">

	<div class="container ">

		<div class="row pt-5 pb-5">

			<div class="col-md-3">widget</div>
			<div class="col-md-3">widget</div>
			<div class="col-md-3">widget</div>
			<div class="col-md-3">widget</div>		
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php

				wp_nav_menu( array(
					'menu' =>	'footer',
					'theme_location' => 'menu-1',
					'menu_class'        => 'navbar-nav',
					'container'			=> 'nav',
					'container_class'  => 'navbar navbar-expand-sm ',
					'depth'          => 3,
				));
				?>
			</div>
			<div class="col-md-6">
				<?php 
				wp_nav_menu( array(
					'menu' =>	'redes footer',
					'theme_location' => 'menu-1',
					'menu_class'        => 'navbar-nav ml-auto',
					'container'			=> 'nav',
					'container_class'  => 'navbar navbar-expand-sm ',
					'depth'          => 3,
				));

				?>
			</div>
		</div>
	</div>


	<!-- 	<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'oreninc' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'oreninc' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'oreninc' ), 'oreninc', '<a href="https://ricv.pe">ricv</a>' );
				?>
			</div><!-- .site-info --> 
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>

</body>
</html>
