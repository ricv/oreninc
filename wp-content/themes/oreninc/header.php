<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package oreninc
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'oreninc' ); ?></a>

		<header id="masthead" class="site-header">
			<?php 
			$custom_logo_id = get_theme_mod( 'custom_logo' );
			$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
			?>
			<nav class="navbar navbar-expand-lg navbar-light bg-light" style="top: initial">
				<div class="container">
					<a class="navbar-brand mr-auto" href="<?php echo get_site_url(); ?>">
						<img src="<?php echo $image[0]; ?> " alt="">
						<?php 
						if ( is_front_page() && is_home() ){
							?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php
						}else {
							?>
							<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
							<?php
						}
						$oreninc_description = get_bloginfo( 'description', 'display' );
						if ( $oreninc_description || is_customize_preview() ) {
							?>
							<p class="site-description"><?php echo $oreninc_description; /* WPCS: xss ok. */ ?></p>
						<?php }  ?>
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<?php

					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_class'        => 'navbar-nav ml-auto',
						'container_class'  => 'collapse navbar-collapse',
						'container_id'    => 'navbarNav',
						'depth'          => 3,
					));
					?>

				</div>
			</nav>
		</header>



	<div id="content" class="site-content bg-oreninc pt-4">
